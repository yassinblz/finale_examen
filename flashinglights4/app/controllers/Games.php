<?php
  class Games extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->gameModel = $this->model('Game');
       $this->userModel = $this->model('User');
       $this->teamModel = $this->model('Team');
       $this->leagueModel = $this->model('League');


    }
       
       public function game(){
        
        $game = $this->gameModel->getGames();
        $data = [
            'games' => $game
        ];
        $this->view('competition/game/game', $data);

    }



    public function insertingone(){
    
        // dit zal checken of het om een POST actie gaat 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
    // het formulier uitvoeren
    
    //Sanitize post data 
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    $teams = $this->teamModel->getTeams();
    $leagues = $this->leagueModel->getLeagues();

    $data =[
        'Date' => trim($_POST['Date']),
        'Status' => trim($_POST['Status']),
        'ScoreHome' => trim($_POST['ScoreHome']),
        'ScoreVisitors' => trim($_POST['ScoreVisitors']),
        'TeamHomeId' => (int)$_POST['TeamHomeId'],
        'TeamVisitorId' => (int)$_POST['TeamVisitorId'],
        'LigaId' => (int)$_POST['LigaId'],
        'Date_err' => '',
        'Status_err' =>'',
        'ScoreHome_err' => '',
        'ScoreVisitors_err' => '',
        'TeamHomeId_err' => '',
        'TeamVisitorId_err' => '',
        'LigaId_err' => '',
        'teams' => $teams,
        'leagues' => $leagues,
    ];
    
    // validate name
    if(empty($data['Date'])){
        $data['Date_err'] = 'Vul een datum in';
    }
    // validate location
    if(empty($data['Status'])){
        $data['Status_err'] = 'Vul een status in';
    }
    // validate score
    if(empty($data['ScoreHome'])){
        $data['ScoreHome_err'] = 'Vul een score in';
    }
    // validate score
    if(empty($data['ScoreVisitors'])){
        $data['ScoreVisitors_err'] = 'Vul een score in';
    }

    if(empty($data['TeamHomeId'])){
        $data['TeamHomeId_err'] = 'Selecteer een thuisploeg';
    }
 
    if(empty($data['TeamVisitorId'])){
        $data['TeamVisitorId_err'] = 'Selecteer een bezoekersploeg';
    }

    if(empty($data['LigaId'])){
        $data['LigaId_err'] = 'Selecteer een liga';
    }
    // zorg dat alle error's leeg zijn
    if(empty($data['Date_err']) && empty($data['Status_err']) && empty($data['ScoreHome_err']) && empty($data['ScoreVisitors_err']))
    {
        // validated
       // die('SUCCESS');
    
    if($this->gameModel->insertingone($data)){
        flash('register_success', 'De wedstrijd werd toegevoegd');
        redirect('games/game');
        
    } else{
        die('Something went wrong');
    }
    
    } else{

        
        // load view with errors
        $this->view('competition/game/insertingone', $data);
    
    }
    
    
     } else {
            // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
            //echo 'laadt de spelerslijst';
    //initialiseer data 

    $teams = $this->teamModel->getTeams();
    $leagues = $this->leagueModel->getLeagues();

    $data =[
        'Date' =>  '',
        'Status' =>  '',
        'ScoreHome' =>  '',
        'ScoreVisitors' =>  '',
        'Date_err' => '',
        'Status_err' =>'',
        'ScoreHome_err' => '',
        'ScoreVisitors_err' => '',
        'TeamHomeId_err' => '',
        'TeamVisitorId_err' => '',
        'LigaId_err' => '',
        'teams' => $teams,
        'leagues' => $leagues,
    ];
    
    // load view
    $this->view('competition/game/insertingone', $data);

     }
    }




    public function showGame($id){

        $game = $this->gameModel->getGameById($id);
        $team = $this->teamModel->getTeamById($game->TeamHomeId);
        $teams = $this->teamModel->getTeamById($game->TeamVisitorId);
        $league = $this->leagueModel->getLeagueById($game->LigaId);

    
        $data = [
            'game' => $game,
            'team' => $team,
            'teams' => $teams,
            'league' => $league,
        ];
    
    $this->view('competition/game/showGame', $data);
    
    }

    public function editGame($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $teams = $this->teamModel->getTeams();
            $leagues = $this->leagueModel->getLeagues();
            $data = [
    
                // 'Id' => $id,
                'Date' => trim($_POST['Date']),
                'ScoreHome' => trim($_POST['ScoreHome']),
                'ScoreVisitors' => trim($_POST['ScoreVisitors']),
                'Status' => trim($_POST['Status']),
                'TeamHomeId' => (int)$_POST['TeamHomeId'],
                'TeamVisitorId' => (int)$_POST['TeamVisitorId'],
                'LigaId' => (int)$_POST['LigaId'],
                'teams' => $teams,
                'leagues' => $leagues,

                        ];
                        // validate data
                    
                        if(empty($data['Date'])){
                            $data['Date_err'] = 'Selecteer een datum';
                        }
                        if(empty($data['ScoreHome'])){
                            $data['ScoreHome_err'] = 'Vul een score in';
                        }
                        if(empty($data['ScoreVisitors'])){
                            $data['ScoreVisitors_err'] = 'Vul een score in';
                        }

                        if(empty($data['Status'])){
                            $data['Status_err'] = 'Vul een status in';
                        }
    
                        if(empty($data['TeamHomeId'])){
                            $data['TeamHomeId_err'] = 'Selecteer een thuisploeg';
                        }
                     
                        if(empty($data['TeamVisitorId'])){
                            $data['TeamVisitorId_err'] = 'Selecteer een bezoekersploeg';
                        }

                        if(empty($data['LigaId'])){
                            $data['LigaId_err'] = 'Selecteer een liga';
                        }
                        // make sure there are no errors  
                        if(empty($data['Date_err']) && empty($data['ScoreHome_err']) && empty($data['ScoreVisitors_err']) && empty($data['Status_err'])  && empty($data['TeamHomeId_err'])  && empty($data['TeamVisitorId_err']) && empty($data['LigaId_err'])){
                            // validated
                            if($this->gameModel->updateGame($data)){
                                
                                // hier is not iets niet juist...
                                flash('register_success', 'De wedstrijd werd aangepast');
                                redirect('games/game'); 
    
                            }else{
                                die('Er ging iets mis');
                            }
    
                        }else {
                            // load the view with errors
                            $this->view('competition/game/editGame', $data);
                        }
    
        } else{
    
            // get the existing game from the model
            $game = $this->gameModel->getGameById($id);
    
    
    
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($post->user_id != $_SESSION['user_id']){
            //     redirect('posts');
            // }
            
            $teams = $this->teamModel->getTeams();
            $leagues = $this->leagueModel->getLeagues();

            // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
            $data = [
        'Date' =>$game->Date,
        'Id' =>$game->Id,
        'Status' =>$game->Status,
        'ScoreHome' => $game->ScoreHome,
        'ScoreVisitors' =>$game->ScoreVisitors,
        'TeamHomeId' =>$game->TeamHomeId,
        'TeamVisitorId' =>$game->TeamVisitorId,
        'LigaId' =>$game->LigaId,
        'teams' => $teams,
        'leagues' => $leagues,
            ];
    
            // we nemen dus de id etc..  en geven die mee via $data aan de edit view
            // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
            $this->view('competition/game/editGame', $data);
        }
    }


    public function deleteGame($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // get the existing player from the model
            $game = $this->gameModel->getGameById($id);
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($player->user_id != $_SESSION['user_id']){
            //    redirect('footballs');
            // }
            if($this->gameModel->deleteGame($id)){
                flash('register_success', 'De wedstrijd werd verwijderd');
                redirect('games/game');
            }else {
                die('Something went wrong');
            }
        }else {
            redirect('posts');
        }
    
    }



    }