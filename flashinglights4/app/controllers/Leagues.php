<?php
  class Leagues extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->leagueModel = $this->model('League');
       $this->userModel = $this->model('User');
        
       

    }

    public function league(){
        
        

        $league = $this->leagueModel->getLeagues();
        $data = [
            'leagues' => $league
        ];
        $this->view('competition/league/league', $data);




    }




    public function insertingone(){
    
        // dit zal checken of het om een POST actie gaat 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
    // het formulier uitvoeren
    
    //Sanitize post data 
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    $data =[
        'Name' => trim($_POST['Name']),
        'Year' => (int)($_POST['Year']),
        'IsInPlanning' => $_POST['IsInPlanning'] == '0' ? false : true,
        'Name_err' => '',
        'Year_err' =>'',
        'IsInPlanning_err' => ''
    ];
    
    // validate name
    if(empty($data['Name'])){
        $data['Name_err'] = 'Kies een naam voor de liga';
    }
    // validate location
    if(empty($data['Year'])){
        $data['Year_err'] = 'Kies een speeljaar voor de liga';
    }
    // validate score
    if(empty($data['IsInPlanning'])){
        $data['IsInPlanning_err'] = 'Staat de liga gepland?';
    }

    
    // zorg dat alle error's leeg zijn
    if(empty($data['Name_err']) && empty($data['Year_err']))
    // && empty($data['IsInPlanning_err'])  ==> Niet nodig, want default waarde = 0
    {
        // validated
       // die('SUCCESS');
    
    if($this->leagueModel->insertingone($data)){
        flash('register_success', 'De liga werd toegevoegd');
        redirect('leagues/league');
        
    } else{
        die('Something went wrong');
    }
    
    } else{
        // load view with errors
        $this->view('competition/league/insertingone', $data);
    
    }
    
    
     } else {
            // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
            //echo 'laadt de spelerslijst';
    //initialiseer data 
    $data =[
        'Name' =>  '',
        'Year' =>  '',
        'IsInPlanning' =>  '',
        'Name_err' => '',
        'Year_err' =>'',
        'IsInPlanning_err' => ''

    ];
    
    // load view
    $this->view('competition/league/insertingone', $data);
    
    

  
    
        }
    }

    public function showLeague($id){

        $league = $this->leagueModel->getLeagueById($id);
        
    
        $data = [
            'league' => $league,
            
    
        ];
    
    $this->view('competition/league/showLeague', $data);
    
    }



    public function editLeague($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // SANITIZE POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
    
                'Id' => $id,
                'Name' => trim($_POST['Name']),
                'Year' => (int)($_POST['Year']),
                'IsInPlanning' => $_POST['IsInPlanning'] == '0' ? false : true,
                
                        ];
                        // validate data
                        // validate title
                        if(empty($data['Name'])){
                            $data['Name_err'] = 'Kies een naam voor de liga';
                        }
                        if(empty($data['Year'])){
                            $data['Year_err'] = 'Kies een speeljaar voor de liga';
                        }
                        if(empty($data['IsInPlanning'])){
                            $data['IsInPlanning_err'] = 'Staat de liga gepland?';
                        }

                     
    
                        // make sure there are no errors  
                        if(empty($data['Name_err']) && empty($data['Year_err']) ){
                            // && empty($data['IsInPlanning_err']) ==> Niet nodig, want default waarde = 0

                            // validated
                            if($this->leagueModel->updateLeague($data)){
                                
                                // hier is nog iets niet juist...
                                flash('register_success', 'De liga werd aangepast');
                                redirect('leagues/league'); 

    
                            }else{
                                die('Something went wrong');
                            }
    
                        }else {
                            // load the view with errors
                            $this->view('competition/league/editLeague', $data);
                        }
    
        } else{
    
            // get the existing player from the model
            $league = $this->leagueModel->getLeagueById($id);
    
    
    
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($post->user_id != $_SESSION['user_id']){
            //     redirect('posts');
            // }
    
    
            // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
            $data = [
        'Name' =>$league->Name,
        'Year' =>$league->Year,
        'IsInPlanning' =>$league->IsInPlanning,
        'Id' =>$league->Id      
            ];
    
            // we nemen dus de id etc..  en geven die mee via $data aan de edit view
            // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
            $this->view('competition/league/editLeague', $data);
        }
    }


    public function deleteLeague($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // get the existing player from the model
            $league = $this->leagueModel->getLeagueById($id);
            // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
            // if($player->user_id != $_SESSION['user_id']){
            //    redirect('footballs');
            // }
            if($this->leagueModel->deleteLeague($id)){
                flash('register_success', 'De liga werd werd verwijderd');
                redirect('leagues/league');
            }else {
                die('Something went wrong');
            }
        }else {
            redirect('posts');
        }
    
    }




}