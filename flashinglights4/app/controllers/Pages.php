<?php
  class Pages extends Controller {
    public function __construct(){
       

    }

    public function index(){

        if(isLoggedIn()){
            redirect('posts');
        }
        else{  
            redirect('users/login');
            
        }

       
        $data = [
            'title' => 'Yassin Competition',
            'description' => 'Yassin zijn competition pagina'
                ];

        $this->view('pages/index', $data);
      
    }

    public function about(){
        $data = ['title' => 'Over mezelf',
        'description' => 'Dit is een about pagina gebouwd tijdens de lessen van Traversy MVC in Udemy course'];

        $this->view('pages/about', $data);
      
    }
    public function home(){
        
        $data = ['title' => 'Thuis',
        'description' => 'Dit is een homepagina die gebouwd werd tijdens de lessen van Traversy MVC in Udemy course'];

        $this->view('pages/home', $data);
    }



      
  }