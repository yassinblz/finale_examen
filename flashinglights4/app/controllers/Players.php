<?php
  class Players extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->playerModel = $this->model('Player');
       $this->userModel = $this->model('User');
       $this->teamModel = $this->model('Team');
        
       

    }

    public function index(){
        
        $data = [
            'title' => 'Yassin zijn examen',
            'description' => 'Dit is mijn examen'
                ];

        $this->view('competition/index', $data);
      
    }


    // een controller heeft een function nodig, dit moet je toevoegen voor elke pagina die je maakt. 
    public function player(){
        

        $player = $this->playerModel->getPlayers();
        $data = [
            'players' => $player
        ];
        $this->view('competition/player/player', $data);
      //  $this->view('competition/player/insertingone', $data);  --> niet nodig hier




    }




   

public function insertingone(){
    
    // dit zal checken of het om een POST actie gaat 
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
// het formulier uitvoeren

//Sanitize post data 
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$teams = $this->teamModel->getTeams();


$data =[
    'FirstName' => trim($_POST['FirstName']),
    'LastName' => trim($_POST['LastName']),
    'Email' => trim($_POST['Email']),
    'Address1' => trim($_POST['Address1']),
    'Address2' =>trim($_POST['Address2']),
    'PostalCode' =>trim($_POST['PostalCode']),
    'City' =>trim($_POST['City']),
    'Country' =>trim($_POST['Country']),
    'Phone' =>trim($_POST['Phone']),
    'Birthday' =>trim($_POST['Birthday']),
    'TeamId' => (int)$_POST['TeamId'],
    'FirstName_err' => '',
    'LastName_err' =>'',
    'Email_err' => '',
    'Address1_err' =>'',
    'Address_err' => '',
    'PostalCode_err' => '',
    'City_err' => '',
    'Country_err' => '',
    'Phone_err' =>'',
    'Birthday_err' => '',
    'teams' => $teams,

];

// validate firstname
if(empty($data['FirstName'])){
    $data['FirstName_err'] = 'Vul een voornaam in';
}
// validate lastname
if(empty($data['LastName'])){
    $data['LastName_err'] = 'Vul een achternaam in';
}
// validate email
if(empty($data['Email'])){
    $data['Email_err'] = 'Vul een emailadres in';
}
// validate address1
if(empty($data['Address1'])){
    $data['Address1_err'] = 'Vul een hoofdadres in';
}
// validate address2
if(empty($data['Address2'])){
    $data['Address2_err'] = 'Vul een tweede adres in';
}
// validate postalcode
if(empty($data['PostalCode'])){
    $data['PostalCode_err'] = 'Vul een postcode in';
}
// validate city
if(empty($data['City'])){
    $data['City_err'] = 'Vul een stad in';
}
// validate country
if(empty($data['Country'])){
    $data['Country_err'] = 'Vul een land in';
}
// validate Phone
if(empty($data['Phone'])){
    $data['Phone_err'] = 'Vul een telefoonnumer in';
}
// validate birthday
if(empty($data['Birthday'])){
    $data['Birthday_err'] = 'Vul een geboortedatum in';
}

// zorg dat alle error's leeg zijn
if(empty($data['FirstName_err']) && empty($data['LastName_err']) && empty($data['Email_err']) && 
empty($data['Address1_err']) && empty($data['Address2_err']) && empty($data['PostalCode_err']) && 
empty($data['City_err']) && empty($data['Country_err']) && empty($data['Phone_err']) && empty($data['Birthday_err']))
{
    // validated
   // die('SUCCESS');

if($this->playerModel->insertingone($data)){
    flash('register_success', 'De speler werd toegevoegd');
    redirect('players/player');
} else{
    die('Something went wrong');
}





} else{
    // load view with errors
    $this->view('competition/player/insertingone', $data);

}


 } else {
        // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
        //echo 'laadt de spelerslijst';
//initialiseer data 

//laadt de spelerlijst en initialiseer de data
$teams = $this->teamModel->getTeams();

$data =[
    'FirstName' =>  '',
    'LastName' =>  '',
    'Email' =>  '',
    'Address1' =>  '',
    'Address2' => '',
    'PostalCode' => '',
    'City' => '',
    'Country' => '',
    'Phone' => '',
    'Birthday' => '',
    'Id' => '',
    'TeamId' => '',
    'teams' => $teams,
    'FirstName_err' => '',
    'LastName_err' =>'',
    'Email_err' => '',
    'Address1_err' =>'',
    'Address_err' => '',
    'PostalCode_err' => '',
    'City_err' => '',
    'Country_err' => '',
    'Phone_err' =>'',
    'Birthday_err' => ''
];

// load view
$this->view('competition/player/insertingone', $data);


    }
}



public function showPlayer($id){

    $player = $this->playerModel->getPlayerById($id);
    $teams = $this->teamModel->getTeamById($player->TeamId);
    

    $data = [
        'player' => $player,
        'team' => $teams
    ];

$this->view('competition/player/showPlayer', $data);

}






public function editPlayer($id){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // SANITIZE POST array
        
        
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        

        $teams = $this->teamModel->getTeams();

        $data = [

            'Id' => $id,
            'FirstName' => trim($_POST['FirstName']),
            'LastName' => trim($_POST['LastName']),
            'Email' => trim($_POST['Email']),
            'Address1' => trim($_POST['Address1']),
            'Address2' => trim($_POST['Address2']),
            'PostalCode' => trim($_POST['PostalCode']),
            'City' => trim($_POST['City']),
            'Country' => trim($_POST['Country']),
            'Phone' => trim($_POST['Phone']),
            'Birthday' => trim($_POST['Birthday']),
            'TeamId' => (int)$_POST['TeamId'],
            'teams' => $teams,

                    ];
                    // validate data
                    // validate title
                    if(empty($data['FirstName'])){
                        $data['FirstName_err'] = 'Vul een voornaam in';
                    }
                    if(empty($data['LastName'])){
                        $data['LastName_err'] = 'Vul een achternaam in';
                    }
                    if(empty($data['Email'])){
                        $data['Email_err'] = 'Vul een emailadres in';
                    }
                    if(empty($data['Address1'])){
                        $data['Address1_err'] = 'Vul een hoofdadres in';
                    }
                    if(empty($data['Address2'])){
                        $data['Address2_err'] = 'Vul een tweede adres in';
                    }
                    if(empty($data['PostalCode'])){
                        $data['PostalCode_err'] = 'Vul een postcode in';
                    }
                    if(empty($data['City'])){
                        $data['City_err'] = 'Vul een stad in';
                    }
                    if(empty($data['Country'])){
                        $data['Country_err'] = 'Vul een land in';
                    }
                    if(empty($data['Phone'])){
                        $data['Phone_err'] = 'Vul een telefoonnumer in';
                    }
                    if(empty($data['Birthday'])){
                        $data['Birthday_err'] = 'Vul een geboortedatum in';
                    }
                 

                    // make sure there are no errors  
                    if(empty($data['FirstName_err']) && empty($data['LastName_err']) && empty($data['Email_err']) && empty($data['Address1_err']) && empty($data['Address2_err']) && empty($data['PostalCode_err']) && empty($data['City_err'])&& empty($data['Country_err']) && empty($data['Phone_err']) && empty($data['Birthday_err']) ){
                        // validated
                        if($this->playerModel->updatePlayer($data)){
                            
                            // hier is not iets niet juist...
                            flash('register_success', 'De speler werd aangepast');
                            redirect('players/player'); 

                        }else{
                            die('Something went wrong');
                        }

                    }else {
                        // load the view with errors
                        $this->view('competition/player/editPlayer', $data);
                    }

    } else{

        // get the existing player from the model
        $player = $this->playerModel->getPlayerById($id);



        // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
        // if($post->user_id != $_SESSION['user_id']){
        //     redirect('posts');
        // }

        $teams = $this->teamModel->getTeams();
        // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
        $data = [
    'FirstName' =>  $player->FirstName,
    'LastName' =>$player->LastName,
    'Email' =>  $player->Email,
    'Address1' =>  $player->Address1,
    'Address2' => $player->Address2,
    'PostalCode' => $player->PostalCode,
    'City' => $player->City,
    'Country' => $player->Country,
    'Phone' => $player->Phone,
    'Birthday' => $player->Birthday,
    'Id' => $player->Id,
    'TeamId' => $player->TeamId,
    'teams' => $teams,
        ];

        // we nemen dus de id etc..  en geven die mee via $data aan de edit view
        // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
        $this->view('competition/player/editPlayer', $data);
    }
}





public function deletePlayer($id){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // get the existing player from the model
        $player = $this->playerModel->getPlayerById($id);
        // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
        // if($player->user_id != $_SESSION['user_id']){
        //    redirect('footballs');
        // }
        if($this->playerModel->deletePlayer($id)){
            flash('register_success', 'De speler werd verwijderd');
            redirect('players/player');
        }else {
            die('Something went wrong');
        }
    }else {
        redirect('posts');
    }

}

  }