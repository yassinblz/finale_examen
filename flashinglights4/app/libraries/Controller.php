<?php
  /*
   * dit is de  Base Controller
   * Loads the models and views
   * je gaat nog andere class maken en dan moet je deze deze extenden.. je nieuwe klasse moet overerven van deze base class.
   * 
   * deze base controller heeft 2 functions, model en view, dus via deze controller kan je nieuwe controller models en views ophalen om dat terug te 
   * geven aan de gebruiker
   */
  class Controller {
    // Load model
    public  function model($model){
      // Require model file 
     require_once '../app/models/' . $model . '.php';

      // Instantiate model
      return new $model();
    }

    // Load view
    public function view($view, $data = []){
      // Check for view file
      if(file_exists('../app/views/' . $view . '.php')){
        require_once '../app/views/' . $view . '.php';
      } else {
        // View does not exist
        die('View does not exist');
      }
    }
  }
