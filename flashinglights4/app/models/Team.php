<?php
class Team {
    private $db;

    public function  __construct(){
        $this->db = new Database;
    }

    public function getTeams(){
        $this->db->query('SELECT * FROM Team');
    
        $results = $this->db->resultSet();
    
        return $results;
    
    }





    public function insertingone($data){
        $this->db->query('INSERT INTO Team (Name, Location, Score ) VALUES(:Name, :Location, :Score)');
        // Bind values
        $this->db->bind(':Name', $data['Name']);
        $this->db->bind(':Location', $data['Location']);
        $this->db->bind(':Score', $data['Score']);

    // Execute
    if($this->db->execute()){
    return true;
    } else {
        return false;
    }
    
}




public function getTeamById($id){ 
    $this->db->query('SELECT * FROM Team WHERE Id = :Id');
    $this->db->bind(':Id', $id);

    $row = $this->db->single();

    return $row;


}

public function updateTeam($data){
        
    $this->db->query('UPDATE Team SET Name = :Name, Location = :Location, Score = :Score WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $data['Id']);
    $this->db->bind(':Name', $data['Name']);
    $this->db->bind(':Location', $data['Location']);
    $this->db->bind(':Score', $data['Score']);

// Execute
if($this->db->execute()){
return true;
} else {
    return false;
}
}


public function deleteTeam($id){
            
        
    $this->db->query('DELETE FROM Team WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $id);

    // Execute
    if($this->db->execute()){
return true;
} else {
    return false;
}
    
}



}