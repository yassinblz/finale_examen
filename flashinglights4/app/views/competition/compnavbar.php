
<nav class="navbar navbar-expand-lg navbar-dark">

   <ul class="navbar-nav mr-auto">
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/players/player">Speler</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/teams/team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/games/game">Wedstrijd</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/leagues/league">Liga</a>
          </li>
  
        </ul>
      
    </div>
  </nav>
