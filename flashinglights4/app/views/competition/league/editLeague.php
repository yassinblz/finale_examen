<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    


  
   <a href="<?php echo URLROOT; ?>/leagues/league" class="btn btn-light">Annuleer</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Liga aanpassen</h2>
        <p>Gebruik dit formulier om een liga aan te passen</p>
        <form action="<?php echo URLROOT; ?>/leagues/editleague/<?php echo $data['Id']; ?>" method="post">
          
          <div class="form-group">
            <label for="Name">Naam: <sup>*</sup></label>
            <input type="text" name="Name" class="form-control form-control-lg <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Name']; ?>">
            <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Year">Jaar: <sup>*</sup></label>
            <input type="number" min="1000" max="9999" name="Year" class="form-control form-control-lg <?php echo (!empty($data['Year_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Year']; ?>">
            <span class="invalid-feedback"><?php echo $data['Year_err']; ?></span>
          </div>
          <!-- <div class="form-group">
            <label for="IsInPlanning">Is In Planning: <sup>*</sup></label>
            <input type="text" name="IsInPlanning" class="form-control form-control-lg <?php echo (!empty($data['IsInPlanning_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['IsInPlanning']; ?>">
            <span class="invalid-feedback"><?php echo $data['IsInPlanning_err']; ?></span>
          </div> -->

          <div class="form-group">
            <label for="IsInPlanning">Is dit ingepland? <sup>*</sup></label>
            <input type="radio" name="IsInPlanning" id="IsInPlanningYes" value="1"
                        <?php echo $data['IsInPlanning'] == 1 ? ' checked ' : '';?>><label>Ja</label>
                    <input type="radio" name="IsInPlanning" id="IsInPlanningNo" value="0"
                        <?php echo $data['IsInPlanning'] == 0 ? ' checked ' : '';?>><label>Nee</label>


          </div>
          <!-- <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg < ?php echo (!empty($data
            //['body_err'])) ? 'is-invalid' : ''; ?>">< ?php echo $data //[ 'body'];?></textarea>
            <span class="invalid-feedback">< ?php echo $data //['body_err']; ?></span>
          </div> -->
        <input type='submit' class="btn btn-success" value="Liga aanpassen">
        </form>
    </div>
    </article>

<nav>Dit is een zijbalk</nav>

<aside>


</aside>


</main>
<footer>

<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>


</body>
