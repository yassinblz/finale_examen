<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>

 
</header>
<main>
    
  <article>
      
  <?php require APPROOT . '/views/competition/compnavbar.php' ?>


        
        <h2 >Voeg een liga toe</h2>
        <p id=text>Vul onderstaande formulier om een liga toe te voegen</p>
        
        
        <!-- <form action="< ?php echo URLROOT; ?>/competition/team/insertingone" method="post">    deze fout had ik gemaakt waar ik uren op gezocht heb. ik had naar de view directory
        verwezen ipv naar de controler. hierdoor kreeg ik geen enkele error pagina te zien en werd er ook niks in de db geschreven-->



        <form action="<?php echo URLROOT; ?>/leagues/insertingone" method="post">
          <div class="form-group">
            <label for="Name">Naam: <sup>*</sup></label>
            <input type="text" name="Name" class="form-control form-control-lg <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Name']; ?>">
            <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Year">Jaar: <sup>*</sup></label>
            <input type="number" placeholder="YYYY" min="1000" max="9999" name="Year" class="form-control form-control-lg <?php echo (!empty($data['Year_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Year']; ?>">
            <span class="invalid-feedback"><?php echo $data['Year_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="IsInPlanning">Is dit ingepland?  <sup>*</sup></label>
            <input type="radio" name="IsInPlanning" id="IsInPlanningYes" value="1"><label>Ja</label>
            <input type="radio" name="IsInPlanning" id="IsInPlanningNo" value="0"><label>Nee</label>


          </div>
         

          <div class="row">
            <div class="col">
              <input type="submit" value="Liga toevoegen" class="btn btn-success btn-block">
            </div>
          
          </div>
        </form>
   

</div>
  
  </article>

  <nav>Dit is een zijbalk</nav>

<aside>


</aside>


</main>
<footer>

<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>


</body>
  
