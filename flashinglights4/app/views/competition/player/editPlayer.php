<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    


  
   <a href="<?php echo URLROOT; ?>/players/player" class="btn btn-light">Annuleer</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Speler aanpassen</h2>
        <p>Gebruik dit formulier om de speler aan te passen</p>
        <form action="<?php echo URLROOT; ?>/players/editplayer/<?php echo $data['Id']; ?>" method="post">
          
          <div class="form-group">
            <label for="FirstName">Voornaam: <sup>*</sup></label>
            <input type="text" name="FirstName" class="form-control form-control-lg <?php echo (!empty($data['FirstName_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['FirstName']; ?>">
            <span class="invalid-feedback"><?php echo $data['FirstName_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="LastName">Achternaam: <sup>*</sup></label>
            <input type="text" name="LastName" class="form-control form-control-lg <?php echo (!empty($data['LastName_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['LastName']; ?>">
            <span class="invalid-feedback"><?php echo $data['LastName_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Email">E-mail: <sup>*</sup></label>
            <input type="text" name="Email" class="form-control form-control-lg <?php echo (!empty($data['Email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Email']; ?>">
            <span class="invalid-feedback"><?php echo $data['Email_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Address1">Address 1: <sup>*</sup></label>
            <input type="text" name="Address1" class="form-control form-control-lg <?php echo (!empty($data['Address1_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Address1']; ?>">
            <span class="invalid-feedback"><?php echo $data['Address1_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Address2">Address 2: <sup>*</sup></label>
            <input type="text" name="Address2" class="form-control form-control-lg <?php echo (!empty($data['Address2_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Address2']; ?>">
            <span class="invalid-feedback"><?php echo $data['Address2_err']; ?></span>
          </div>
<div class="form-group">
            <label for="PostalCode">Postcode: <sup>*</sup></label>
            <input type="number" min="1000" max="9999" name="PostalCode" class="form-control form-control-lg <?php echo (!empty($data['PostalCode_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['PostalCode']; ?>">
            <span class="invalid-feedback"><?php echo $data['PostalCode_err']; ?></span>
          </div>
<div class="form-group">
            <label for="City">Stad: <sup>*</sup></label>
            <input type="text" name="City" class="form-control form-control-lg <?php echo (!empty($data['City_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['City']; ?>">
            <span class="invalid-feedback"><?php echo $data['City_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Country">Land: <sup>*</sup></label>
            <input type="text" name="Country" class="form-control form-control-lg <?php echo (!empty($data['Country_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Country']; ?>">
            <span class="invalid-feedback"><?php echo $data['Country_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Phone">Telefoonnummer: <sup>*</sup></label>
            <input type="text" name="Phone" class="form-control form-control-lg <?php echo (!empty($data['Phone_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Phone']; ?>">
            <span class="invalid-feedback"><?php echo $data['Phone_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Birthday">Geboortedatum: <sup>*</sup></label>
            <input type="text" name="Birthday" class="form-control form-control-lg <?php echo (!empty($data['Birthday_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Birthday']; ?>">
            <span class="invalid-feedback"><?php echo $data['Birthday_err']; ?></span>
          </div>
          <!-- <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg < ?php echo (!empty($data
            //['body_err'])) ? 'is-invalid' : ''; ?>">< ?php echo $data //[ 'body'];?></textarea>
            <span class="invalid-feedback">< ?php echo $data //['body_err']; ?></span>
          </div> -->




          <div class="dropdown">
            <label for="TeamId">Team: <sup>*</sup> </label>
                        <select id="TeamId" name="TeamId" class="form-control form-control-lg " style="width: 100%">
                             <?php
                                if ($data) {
                                    foreach ($data['teams'] as $team) {
                                        $selected = ($team->Id == $data['TeamId']) ? 'selected' : '';
                            ?>
                                    <option value="<?php echo $team->Id . ' / '. $data['TeamId'];?>" <?php echo $selected?>>
                                        <?php echo $team->Name; ?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                            </select>
          </div>
          <p> </p>
        <input type='submit' class="btn btn-success" value="Speler aanpassen">
        </form>
    </div>

    </article>

<nav> Dit is een zij balk



</nav>

<aside>





</aside>

</main>



<footer>
<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>

</body>

