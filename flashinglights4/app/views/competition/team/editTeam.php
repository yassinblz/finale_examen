<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    


  
   <a href="<?php echo URLROOT; ?>/teams/team" class="btn btn-light">Annuleer</a>
      <div class="card card-body bg-light mt-5">
   
        <h2>Pas een team aan</h2>
        <p>Gebruik dit formulier om een team aan te passen</p>
        <form action="<?php echo URLROOT; ?>/teams/editteam/<?php echo $data['Id']; ?>" method="post">
          
          <div class="form-group">
            <label for="Name">Naam: <sup>*</sup></label>
            <input type="text" name="Name" class="form-control form-control-lg <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Name']; ?>">
            <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="Location">Locatie: <sup>*</sup></label>
            <input type="text" name="Location" class="form-control form-control-lg <?php echo (!empty($data['Location_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Location']; ?>">
            <span class="invalid-feedback"><?php echo $data['Location_err']; ?></span>
          </div>
<div class="form-group">
            <label for="Score">Score: <sup>*</sup></label>
            <input type="number" min="0" max="9999" name="Score" class="form-control form-control-lg <?php echo (!empty($data['Score_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['Score']; ?>">
            <span class="invalid-feedback"><?php echo $data['Score_err']; ?></span>
          </div>

        <input type='submit' class="btn btn-success" value="Team aanpassen">
        </form>
    </div>

    </article>

<nav>zij navigatie</nav>

<aside>







</aside>




</main>
<footer>

<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>


</body>
