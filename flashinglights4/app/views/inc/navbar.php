
<nav class="navbar navbar-expand-lg navbar-dark">
  <div class="container">
      <a class="navbar-brand" href="<?php echo URLROOT; ?>/pages/home"><?php echo SITENAME; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">



        <ul class="navbar-nav mr-auto">
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/pages/home">Thuis</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/players/index">Competitie</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>">Posts</a>
          </li>
          
          <li class="nav-item">
          <a class="nav-link" onclick="goBack()">Ga terug</a>
        </li>
        </ul>
        
        <ul class="navbar-nav ml-auto">
        <?php if(isset($_SESSION['user_id'])) : ?>
        <li class="nav-item">
            <a class="nav-link" href="#">Welkom <?php echo $_SESSION['user_firstname']; ?></a>
          </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Log uit</a>
          </li>
 
          <?php else : ?>
          
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/register">Registreer</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/login">Login</a>
          </li>

         
          
<?php endif; ?>
        </ul>
      </div>
    </div>
  </nav>
