<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    




  
   <a href="<?php echo URLROOT; ?>/posts" class="btn  btn-light">Annuleer</a>
      <div class="card card-body  bg-light mt-5">
   
        <h2>Voeg een post toe</h2>
        <p>Gebruik dit formulier om een post aan te maken</p>
        <form action="<?php echo URLROOT; ?>/posts/add" method="post">
          
          <div class="form-group">
            <label for="title">Titel: <sup>*</sup></label>
            <input type="text" name="title" class="form-control form-control-lg <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['title']; ?>">
            <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" name="password" class="form-control form-control-lg <?php echo (!empty($data
            ['body_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['body'];?></textarea>
            <span class="invalid-feedback"><?php echo $data['body_err']; ?></span>
          </div>
        <input type='submit' class="btn btn-success" value="Bevestig">
        </form>
        </article>

<nav>side nav</nav>

<aside>aside</aside>

</main>



<footer>
<?php require APPROOT . '/views/inc/footer.php'; ?>
</footer>

</body>

