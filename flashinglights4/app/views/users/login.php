<header >
  <?php require APPROOT . '/views/inc/header.php'; ?>
  <?php require APPROOT . '/views/inc/navbar.php' ?>
  
  
</header>
<main>
    
  <article>
    
<div class="login">

  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body bg-light mt-5">
      <?php flash('register_success'); ?>
        <h2>Log in</h2>
        <p>Vul uw logingegevens aub </p>
        <form action="<?php echo URLROOT; ?>/users/login" method="post">
          
          <div class="form-group">
            <label for="email">Email: <sup>*</sup></label>
            <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
            <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="password">Wachtwoord: <sup>*</sup></label>
            <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
            <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
          </div>
          

          <div class="row">
            <div class="col">
              <input type="submit" value="login" class="btn btn-success btn-block">
            </div>
            <div class="col">
              <a href="<?php echo URLROOT; ?>/users/register" class="btn btn-light btn-block">Nog geen admin? Registreer hier!</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>







</div>
  
  </article>

  <nav>side nav</nav>

  <aside>aside</aside>

</main>



<footer>
  footer
</footer>

</body>
  
<?php require APPROOT . '/views/inc/footer.php'; ?>