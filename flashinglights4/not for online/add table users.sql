use Cursist2;
CREATE TABLE `posts`(
	`id` INT (11) NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    `email`VARCHAR(255) NOT NULL,
    `password`VARCHAR(255) NOT NULL,
    `age`INT(3) NOT NULL,
    `location`VARCHAR(50) NOT NULL,
	`created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT PRIMARY KEY(id));